import Logger from "./logger";
import Model from "../orm/model";
import { Model as ORMModel } from "@vuex-orm/core";
import { PluginComponents } from "@vuex-orm/core/lib/plugins/use";
import { downcaseFirstLetter, isEqual, pick, singularize } from "../support/utils";
import Apollo from "../graphql/apollo";
import Database from "@vuex-orm/core/lib/database/Database";
import { Data, Field, GraphQLType, Options } from "../support/interfaces";
import Schema from "../graphql/schema";
import { Mock, MockOptions } from "../test-utils";
import Adapter, { ConnectionMode } from "../adapters/adapter";
import DefaultAdapter from "../adapters/builtin/default-adapter";
const intro: any = {
  data: {
    __schema: {
      types: [
        {
          name: "Query",
          description: "The root query type which gives access points into the data universe.",
          fields: [
            {
              name: "query",
              description:
                "Exposes the root query type nested one level down. This is helpful for Relay 1\nwhich can only query top level fields if they are in a particular form.",
              args: [],
              type: {
                kind: "NON_NULL",
                ofType: {
                  kind: "OBJECT",
                  name: "Query"
                }
              }
            },
            {
              name: "nodeId",
              description:
                "The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`.",
              args: [],
              type: {
                kind: "NON_NULL",
                ofType: {
                  kind: "SCALAR",
                  name: "ID"
                }
              }
            },
            {
              name: "node",
              description: "Fetches an object given its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description: "The globally unique `ID`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Node",
                kind: "INTERFACE"
              }
            },
            {
              name: "allEventsGroups",
              description: "Reads and enables pagination through a set of `AllEventsGroup`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `AllEventsGroup`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "AllEventsGroupsOrderBy"
                      }
                    }
                  }
                }
              ],
              type: {
                name: "AllEventsGroupsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "contacts",
              description: "Reads and enables pagination through a set of `Contact`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `Contact`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "ContactsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "ContactCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "ContactFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "ContactsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "eventLanguages",
              description: "Reads and enables pagination through a set of `EventLanguage`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `EventLanguage`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "EventLanguagesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventLanguageCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventLanguageFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "EventLanguagesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "events",
              description: "Reads and enables pagination through a set of `Event`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `Event`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "EventsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "EventsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsGroups",
              description: "Reads and enables pagination through a set of `EventsGroup`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `EventsGroup`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "EventsGroupsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsGroupCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsGroupFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "EventsGroupsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsSpeakerProfiles",
              description: "Reads and enables pagination through a set of `EventsSpeakerProfile`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `EventsSpeakerProfile`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "EventsSpeakerProfilesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsSpeakerProfileCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsSpeakerProfileFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "EventsSpeakerProfilesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsStreams",
              description: "Reads and enables pagination through a set of `EventsStream`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `EventsStream`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "EventsStreamsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsStreamCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "EventsStreamFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "EventsStreamsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "featureFlags",
              description: "Reads and enables pagination through a set of `FeatureFlag`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `FeatureFlag`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "FeatureFlagsOrderBy"
                      }
                    }
                  }
                }
              ],
              type: {
                name: "FeatureFlagsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "groups",
              description: "Reads and enables pagination through a set of `Group`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `Group`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "GroupsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "GroupCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "GroupFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "GroupsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileLanguages",
              description:
                "Reads and enables pagination through a set of `SpeakerProfileLanguage`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `SpeakerProfileLanguage`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "SpeakerProfileLanguagesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileLanguageCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileLanguageFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "SpeakerProfileLanguagesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileSocialLinks",
              description:
                "Reads and enables pagination through a set of `SpeakerProfileSocialLink`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `SpeakerProfileSocialLink`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "SpeakerProfileSocialLinksOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileSocialLinkCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileSocialLinkFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "SpeakerProfileSocialLinksConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfiles",
              description: "Reads and enables pagination through a set of `SpeakerProfile`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `SpeakerProfile`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "SpeakerProfilesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "SpeakerProfileFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "SpeakerProfilesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderEvents",
              description: "Reads and enables pagination through a set of `StakeholderEvent`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StakeholderEvent`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholderEventsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderEventCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderEventFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholderEventsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderSocialLinks",
              description: "Reads and enables pagination through a set of `StakeholderSocialLink`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StakeholderSocialLink`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholderSocialLinksOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderSocialLinkCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderSocialLinkFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholderSocialLinksConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderTypes",
              description: "Reads and enables pagination through a set of `StakeholderType`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StakeholderType`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholderTypesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderTypeCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderTypeFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholderTypesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderTypesLanguages",
              description:
                "Reads and enables pagination through a set of `StakeholderTypesLanguage`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StakeholderTypesLanguage`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholderTypesLanguagesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderTypesLanguageCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderTypesLanguageFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholderTypesLanguagesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholders",
              description: "Reads and enables pagination through a set of `Stakeholder`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `Stakeholder`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholdersOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholderFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholdersConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholdersLanguages",
              description: "Reads and enables pagination through a set of `StakeholdersLanguage`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StakeholdersLanguage`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StakeholdersLanguagesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholdersLanguageCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StakeholdersLanguageFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StakeholdersLanguagesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "streamLanguages",
              description: "Reads and enables pagination through a set of `StreamLanguage`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `StreamLanguage`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StreamLanguagesOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StreamLanguageCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StreamLanguageFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StreamLanguagesConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "streams",
              description: "Reads and enables pagination through a set of `Stream`.",
              args: [
                {
                  name: "first",
                  description: "Only read the first `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "last",
                  description: "Only read the last `n` values of the set.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "offset",
                  description:
                    "Skip the first `n` values from our `after` cursor, an alternative to cursor\nbased pagination. May not be used with `last`.",
                  type: {
                    name: "Int",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "before",
                  description: "Read all values in the set before (above) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "after",
                  description: "Read all values in the set after (below) this cursor.",
                  type: {
                    name: "Cursor",
                    kind: "SCALAR"
                  }
                },
                {
                  name: "orderBy",
                  description: "The method to use when ordering `Stream`.",
                  type: {
                    kind: "LIST",
                    ofType: {
                      kind: "NON_NULL",
                      ofType: {
                        kind: "ENUM",
                        name: "StreamsOrderBy"
                      }
                    }
                  }
                },
                {
                  name: "condition",
                  description:
                    "A condition to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StreamCondition",
                    kind: "INPUT_OBJECT"
                  }
                },
                {
                  name: "filter",
                  description:
                    "A filter to be used in determining which values should be returned by the collection.",
                  type: {
                    name: "StreamFilter",
                    kind: "INPUT_OBJECT"
                  }
                }
              ],
              type: {
                name: "StreamsConnection",
                kind: "OBJECT"
              }
            },
            {
              name: "contact",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Contact",
                kind: "OBJECT"
              }
            },
            {
              name: "contactsGroup",
              args: [
                {
                  name: "contactId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "groupId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "ContactsGroup",
                kind: "OBJECT"
              }
            },
            {
              name: "eventLanguage",
              args: [
                {
                  name: "eventId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "language",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "EventLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "event",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Event",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsGroup",
              args: [
                {
                  name: "eventId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "groupId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsGroup",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsSpeakerProfile",
              args: [
                {
                  name: "eventId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "speakerProfileId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsSpeakerProfile",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsStream",
              args: [
                {
                  name: "eventId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "streamId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsStream",
                kind: "OBJECT"
              }
            },
            {
              name: "group",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Group",
                kind: "OBJECT"
              }
            },
            {
              name: "role",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Role",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileLanguage",
              args: [
                {
                  name: "speakerProfileId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "language",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfileLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileSocialLink",
              args: [
                {
                  name: "speakerProfileId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "linkType",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfileSocialLink",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfile",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfile",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderEvent",
              args: [
                {
                  name: "stakeholderId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "eventId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderEvent",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderSocialLink",
              args: [
                {
                  name: "stakeholderId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "linkType",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderSocialLink",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderType",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderType",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderTypesLanguage",
              args: [
                {
                  name: "stakeholderTypeId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "language",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderTypesLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholder",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Stakeholder",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholdersLanguage",
              args: [
                {
                  name: "stakeholderId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "language",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholdersLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "streamLanguage",
              args: [
                {
                  name: "streamId",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                },
                {
                  name: "language",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "String"
                    }
                  }
                }
              ],
              type: {
                name: "StreamLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "stream",
              args: [
                {
                  name: "id",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "UUID"
                    }
                  }
                }
              ],
              type: {
                name: "Stream",
                kind: "OBJECT"
              }
            },
            {
              name: "currentUserApiUserId",
              args: [],
              type: {
                name: "UUID",
                kind: "SCALAR"
              }
            },
            {
              name: "currentUserContactId",
              args: [],
              type: {
                name: "UUID",
                kind: "SCALAR"
              }
            },
            {
              name: "currentUserEventId",
              args: [],
              type: {
                name: "UUID",
                kind: "SCALAR"
              }
            },
            {
              name: "currentUserId",
              args: [],
              type: {
                name: "UUID",
                kind: "SCALAR"
              }
            },
            {
              name: "contactByNodeId",
              description: "Reads a single `Contact` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `Contact`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Contact",
                kind: "OBJECT"
              }
            },
            {
              name: "contactsGroupByNodeId",
              description: "Reads a single `ContactsGroup` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `ContactsGroup`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "ContactsGroup",
                kind: "OBJECT"
              }
            },
            {
              name: "eventLanguageByNodeId",
              description: "Reads a single `EventLanguage` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `EventLanguage`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "EventLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "eventByNodeId",
              description: "Reads a single `Event` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description: "The globally unique `ID` to be used in selecting a single `Event`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Event",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsGroupByNodeId",
              description: "Reads a single `EventsGroup` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `EventsGroup`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsGroup",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsSpeakerProfileByNodeId",
              description: "Reads a single `EventsSpeakerProfile` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `EventsSpeakerProfile`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsSpeakerProfile",
                kind: "OBJECT"
              }
            },
            {
              name: "eventsStreamByNodeId",
              description: "Reads a single `EventsStream` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `EventsStream`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "EventsStream",
                kind: "OBJECT"
              }
            },
            {
              name: "groupByNodeId",
              description: "Reads a single `Group` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description: "The globally unique `ID` to be used in selecting a single `Group`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Group",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileLanguageByNodeId",
              description:
                "Reads a single `SpeakerProfileLanguage` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `SpeakerProfileLanguage`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfileLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileSocialLinkByNodeId",
              description:
                "Reads a single `SpeakerProfileSocialLink` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `SpeakerProfileSocialLink`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfileSocialLink",
                kind: "OBJECT"
              }
            },
            {
              name: "speakerProfileByNodeId",
              description: "Reads a single `SpeakerProfile` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `SpeakerProfile`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "SpeakerProfile",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderEventByNodeId",
              description: "Reads a single `StakeholderEvent` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StakeholderEvent`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderEvent",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderSocialLinkByNodeId",
              description: "Reads a single `StakeholderSocialLink` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StakeholderSocialLink`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderSocialLink",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderTypeByNodeId",
              description: "Reads a single `StakeholderType` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StakeholderType`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderType",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderTypesLanguageByNodeId",
              description:
                "Reads a single `StakeholderTypesLanguage` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StakeholderTypesLanguage`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholderTypesLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholderByNodeId",
              description: "Reads a single `Stakeholder` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `Stakeholder`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Stakeholder",
                kind: "OBJECT"
              }
            },
            {
              name: "stakeholdersLanguageByNodeId",
              description: "Reads a single `StakeholdersLanguage` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StakeholdersLanguage`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StakeholdersLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "streamLanguageByNodeId",
              description: "Reads a single `StreamLanguage` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `StreamLanguage`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "StreamLanguage",
                kind: "OBJECT"
              }
            },
            {
              name: "streamByNodeId",
              description: "Reads a single `Stream` using its globally unique `ID`.",
              args: [
                {
                  name: "nodeId",
                  description:
                    "The globally unique `ID` to be used in selecting a single `Stream`.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "SCALAR",
                      name: "ID"
                    }
                  }
                }
              ],
              type: {
                name: "Stream",
                kind: "OBJECT"
              }
            }
          ]
        },
        {
          name: "Mutation",
          description: "The root mutation type which contains root level fields which mutate data.",
          fields: [
            {
              name: "landertest",
              args: [
                {
                  name: "input",
                  description:
                    "The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "INPUT_OBJECT",
                      name: "LandertestInput"
                    }
                  }
                }
              ],
              type: {
                name: "LandertestPayload",
                kind: "OBJECT"
              }
            },
            {
              name: "logAction",
              args: [
                {
                  name: "input",
                  description:
                    "The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "INPUT_OBJECT",
                      name: "LogActionInput"
                    }
                  }
                }
              ],
              type: {
                name: "LogActionPayload",
                kind: "OBJECT"
              }
            },
            {
              name: "authenticateToEventWithContactToken2",
              args: [
                {
                  name: "input",
                  description: "Authenticate to a specific event with a token",
                  type: {
                    kind: "NON_NULL",
                    ofType: {
                      kind: "INPUT_OBJECT",
                      name: "AuthenticateToEventWithContactToken2Input"
                    }
                  }
                }
              ],
              type: {
                name: "AuthenticationPayload",
                kind: "OBJECT"
              }
            }
          ]
        }
      ]
    }
  }
};

/**
 * Internal context of the plugin. This class contains all information, the models, database, logger and so on.
 *
 * It's a singleton class, so just call Context.getInstance() anywhere you need the context.
 */
export default class Context {
  /**
   * Contains the instance for the singleton pattern.
   * @type {Context}
   */
  public static instance: Context;

  /**
   * Components collection of Vuex-ORM
   * @type {PluginComponents}
   */
  public readonly components: PluginComponents;

  /**
   * The options which have been passed to VuexOrm.install
   * @type {Options}
   */
  public readonly options: Options;

  /**
   * GraphQL Adapter.
   * @type {Adapter}
   */
  public readonly adapter: Adapter;

  /**
   * The Vuex-ORM database
   * @type {Database}
   */
  public readonly database: Database;

  /**
   * Collection of all Vuex-ORM models wrapped in a Model instance.
   * @type {Map<any, any>}
   */
  public readonly models: Map<string, Model> = new Map();

  /**
   * When true, the logging is enabled.
   * @type {boolean}
   */
  public readonly debugMode: boolean = false;

  /**
   * Our nice Vuex-ORM-GraphQL logger
   * @type {Logger}
   */
  public readonly logger: Logger;

  /**
   * Instance of Apollo which cares about the communication with the graphql endpoint.
   * @type {Apollo}
   */
  public apollo!: Apollo;

  /**
   * The graphql schema. Is null until the first request.
   * @type {Schema}
   */
  public schema: Schema | undefined;

  /**
   * Tells if the schema is already loaded or the loading is currently processed.
   * @type {boolean}
   */
  private schemaWillBeLoaded: Promise<Schema> | undefined;

  /**
   * Defines how to query connections. 'auto' | 'nodes' | 'edges' | 'plain' | 'items'
   */
  public connectionMode: ConnectionMode = ConnectionMode.AUTO;

  /**
   * Container for the global mocks.
   * @type {Object}
   */
  private globalMocks: { [key: string]: Array<Mock> } = {};

  /**
   * Private constructor, called by the setup method
   *
   * @constructor
   * @param {PluginComponents} components The Vuex-ORM Components collection
   * @param {Options} options The options passed to VuexORM.install
   */
  private constructor(components: PluginComponents, options: Options) {
    this.components = components;
    this.options = options;

    this.database = options.database;
    this.debugMode = Boolean(options.debug);
    this.logger = new Logger(this.debugMode);
    this.adapter = options.adapter || new DefaultAdapter();

    /* istanbul ignore next */
    if (!options.database) {
      throw new Error("database param is required to initialize vuex-orm-graphql!");
    }
  }

  /**
   * Get the singleton instance of the context.
   * @returns {Context}
   */
  public static getInstance(): Context {
    return this.instance;
  }

  /**
   * This is called only once and creates a new instance of the Context.
   * @param {PluginComponents} components The Vuex-ORM Components collection
   * @param {Options} options The options passed to VuexORM.install
   * @returns {Context}
   */
  public static setup(components: PluginComponents, options: Options): Context {
    this.instance = new Context(components, options);

    this.instance.apollo = new Apollo();
    this.instance.collectModels();

    this.instance.logger.group("Context setup");
    this.instance.logger.log("components", this.instance.components);
    this.instance.logger.log("options", this.instance.options);
    this.instance.logger.log("database", this.instance.database);
    this.instance.logger.log("models", this.instance.models);
    this.instance.logger.groupEnd();

    return this.instance;
  }

  public async loadSchema(): Promise<Schema> {
    if (!this.schemaWillBeLoaded) {
      this.schemaWillBeLoaded = new Promise(async (resolve, reject) => {
        this.logger.log("Fetching GraphQL Schema initially ...");

        this.connectionMode = this.adapter.getConnectionMode();

        // We send a custom header along with the request. This is required for our test suite to mock the schema request.
        const context = {
          headers: { "X-GraphQL-Introspection-Query": "true" }
        };

        const result = intro;

        this.schema = new Schema(result.data.__schema);

        this.logger.log("GraphQL Schema successful fetched", result);

        this.logger.log("Starting to process the schema ...");
        this.processSchema();
        this.logger.log("Schema procession done!");

        resolve(this.schema);
      });
    }

    return this.schemaWillBeLoaded;
  }

  public processSchema() {
    this.models.forEach((model: Model) => {
      let type: GraphQLType;

      try {
        type = this.schema!.getType(model.singularName)!;
      } catch (error) {
        this.logger.warn(`Ignoring entity ${model.singularName} because it's not in the schema.`);
        return;
      }

      model.fields.forEach((field: Field, fieldName: string) => {
        if (!type.fields!.find(f => f.name === fieldName)) {
          this.logger.warn(
            `Ignoring field ${model.singularName}.${fieldName} because it's not in the schema.`
          );

          // TODO: Move skipFields to the model
          model.baseModel.skipFields = model.baseModel.skipFields ? model.baseModel.skipFields : [];
          if (!model.baseModel.skipFields.includes(fieldName)) {
            model.baseModel.skipFields.push(fieldName);
          }
        }
      });
    });

    if (this.connectionMode === ConnectionMode.AUTO) {
      this.connectionMode = this.schema!.determineQueryMode();
      this.logger.log(`Connection Query Mode is ${this.connectionMode} by automatic detection`);
    } else {
      this.logger.log(`Connection Query Mode is ${this.connectionMode} by config`);
    }
  }

  /**
   * Returns a model from the model collection by it's name
   *
   * @param {Model|string} model A Model instance, a singular or plural name of the model
   * @param {boolean} allowNull When true this method returns null instead of throwing an exception when no model was
   *                            found. Default is false
   * @returns {Model}
   */
  public getModel(model: Model | string, allowNull: boolean = false): Model {
    if (typeof model === "string") {
      const name: string = singularize(downcaseFirstLetter(model));
      model = this.models.get(name) as Model;
      if (!allowNull && !model) throw new Error(`No such model ${name}!`);
    }

    return model;
  }

  /**
   * Will add a mock for simple mutations or queries. These are model unrelated and have to be
   * handled  globally.
   *
   * @param {Mock} mock - Mock config.
   */
  public addGlobalMock(mock: Mock): boolean {
    if (this.findGlobalMock(mock.action, mock.options)) return false;
    if (!this.globalMocks[mock.action]) this.globalMocks[mock.action] = [];

    this.globalMocks[mock.action].push(mock);
    return true;
  }

  /**
   * Finds a global mock for the given action and options.
   *
   * @param {string} action - Name of the action like 'simpleQuery' or 'simpleMutation'.
   * @param {MockOptions} options - MockOptions like { name: 'example' }.
   * @returns {Mock | null} null when no mock was found.
   */
  public findGlobalMock(action: string, options: MockOptions | undefined): Mock | null {
    if (this.globalMocks[action]) {
      return (
        this.globalMocks[action].find(m => {
          if (!m.options || !options) return true;

          const relevantOptions = pick(options, Object.keys(m.options));
          return isEqual(relevantOptions, m.options || {});
        }) || null
      );
    }

    return null;
  }

  /**
   * Hook to be called by simpleMutation and simpleQuery actions in order to get the global mock
   * returnValue.
   *
   * @param {string} action - Name of the action like 'simpleQuery' or 'simpleMutation'.
   * @param {MockOptions} options - MockOptions.
   * @returns {any} null when no mock was found.
   */
  public globalMockHook(action: string, options: MockOptions): any {
    let returnValue: null | { [key: string]: any } = null;
    const mock = this.findGlobalMock(action, options);

    if (mock) {
      if (mock.returnValue instanceof Function) {
        returnValue = mock.returnValue();
      } else {
        returnValue = mock.returnValue || null;
      }
    }

    return returnValue;
  }

  /**
   * Wraps all Vuex-ORM entities in a Model object and saves them into this.models
   */
  private collectModels() {
    this.database.entities.forEach((entity: any) => {
      const model: Model = new Model(entity.model as typeof ORMModel);
      this.models.set(model.singularName, model);
      Model.augment(model);
    });
  }
}
